
public class Task5110 {
    public static void main(String[] args) {
      //task 1
      String myString = "Hello Devcamp";
      int count;
      int countRepeat = 0;
      for (int i = 0; i< myString.length(); i++){
        count = 1;
        for (int j = i + 1; j < myString.length(); j++){
          if (myString.charAt(i) == myString.charAt(j) && myString.charAt(i) != ' '){
            count++;
          }
        }
        if (count > 1){
          countRepeat++;
          System.out.println(myString.charAt(i));
         }
      }
      if (countRepeat == 0) {
        System.out.println("Task 1 - No repeated characters!!!");
      } 
      else System.out.println("Task 1 - number of repeated characters are: " + countRepeat);

      //task 2
      String string1 = "Java";
      String string2 = "aavaJ";
      boolean result2 = true;
      for (int i=0; i< string1.length(); i++){
        if (string1.length() != string2.length()){
          result2 = false;
        }
        else if (string1.indexOf(i) != string2.indexOf(string2.length()-1-i)){
          result2 = false;
        }
      }
      System.out.println("Task 2 - Orginal string and Reversed string are same: "+ result2);
      //task 3
      String myString3 = "abcdefa";
      int count3;
      int countRepeat3 = 0;
      for (int i = 0; i< myString.length(); i++){
        count3 = 1;
        for (int j = i + 1; j < myString3.length(); j++){
          if (myString3.charAt(i) == myString3.charAt(j) && myString.charAt(i) != ' '){
            count3++;
          }
        }
        if (count3 > 1){
          countRepeat3++;
         }
      }
      if (countRepeat3 == 0) {
        System.out.println("Task 3 - No repeated characters. 1st unique character is: " + myString3.charAt(0));
      } 
      else System.out.println("Task 3 - unique characters result: No");

      //task 4
      String originalStr = "Hello DevCamp";
      System.out.println("Task 4- Original string: "+ originalStr);    
      String reversedStr = "";

      for (int i = 0; i < originalStr.length(); i++) {
        reversedStr = originalStr.charAt(i) + reversedStr;
      }
      System.out.println("Task 4- Reversed string: "+ reversedStr);
      //task 5
      String myString5 = "linhlt2";
      char[] chars5 = myString5.toCharArray();
      boolean includeNumber = false;
      for(char i : chars5){
         if(Character.isDigit(i)){
          includeNumber = true;
         }
      }
      System.out.println("Task 5 - include number in string: " + includeNumber);
      //task 6
      String myString6 = "linhlt2";
      boolean includeNumber6 = false;
      for(int i=0; i < myString6.length(); i++){
         if(myString6.charAt(i) == 'a' || myString6.charAt(i) == 'e' ||
         myString6.charAt(i) == 'i' || myString6.charAt(i) == 'o' ||
         myString6.charAt(i) == 'u'){
          includeNumber6 = true;
         }
      }
      System.out.println("Task 6- Given string contains vowels: " + includeNumber6);
      //task 7
      String myString7 = "871351";
      int myNumber7 = Integer.parseInt(myString7);
      System.out.println("Task 7 - Number is: " + myNumber7 );
      //task 8
      String myString8 = "DevCamp123 Java";
      String newString8 = myString8.replace('a','e');
      System.out.println("Task 8 - New string is: " + newString8 );
      //task 9
      String myString9[]
            = "I am developer".split(" ");
        String reverseString9 = "";
        for (int i = myString9.length - 1; i >= 0; i--) {
          reverseString9 += myString9[i] + " ";
        }
        System.out.println("Task 9 - Reverse words: " + reverseString9);
        
      //task 10
      String originalStr10 = "ababa1";
      String reversedStr10 = "";
      int length = originalStr10.length();
      for (int i = 0; i < length; i++) {
        reversedStr10 += originalStr10.charAt(length-i-1);
      }
      System.out.println(reversedStr10);
      boolean result10 = true;
      for (int i=0; i< length; i++){
        if (originalStr10.charAt(i) != reversedStr10.charAt(i)){
          result10 = false;
        }
      }
      System.out.println("Task 10 - Orginal string and Reversed string are same: "+ result10);
      
      }
}
